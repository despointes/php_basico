<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Fundamentos básicos com PHP</title>
    <link rel="stylesheet" href="style.css" media="all" />

</head>
<body style="background-color: #0088ff;">
    <Div style="position: absolute; top: 50px; left: 350px; background-color: white;padding: 15px 15px 15px 15px">
        
         <h2>Formulário</h2>
    <form action="./formulario.php" method="get"  class="form">
    <p class="name">
        <label for="nome">Nome</label>
        <input name="nome" type="text" placeholder="Seu Nome" />
    </p>
    
    <p class="email">
        <label for="email">E-mail</label>
        <input name="email" type="text" placeholder="mail@exemplo.com.br" />
    </p>
    
    <p class="idade">
        <label for="idade">Idade</label>
        <input type="number" name="idade">
    </p>
    
    <p class="sexo">
        <label for="sexo">Sexo</label>
        <input class="radio" type="radio" name="sexo" value="F"> <span class="radio_text">Feminino</span><br>
        <input class="radio" type="radio" name="sexo" value="M">  <span class="radio_text">Masculino</span><br>
    </p>
    
    
    <p class="submit">
        <input type="submit" value="Enviar" />
    </p>
    </form>
    
    </Div>
</body>
</html>